package com.alkurop.listtest.network;

import com.alkurop.listtest.model.PhoneNumberModel;

import java.util.List;

import retrofit2.adapter.rxjava.Result;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by alkurop on 9/5/16.
 */
public interface Api {
    @GET("/telemarketing/numbers.json")
    Observable<Result<List<PhoneNumberModel>>> getNumbers();
}
