package com.alkurop.listtest;

/**
 * Created by alkurop on 9/5/16.
 */
public interface MySubscriber<T> {
    void onResult (T result);
    void onError(Throwable t);
    void onTerminate();
}
