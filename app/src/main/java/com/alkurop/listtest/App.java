package com.alkurop.listtest;

import android.app.Application;

import com.github.alkurop.mvpbase.BaseMvpComponent;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by alkurop on 9/5/16.
 */
public class App extends Application {
    @Override
    public void onCreate () {
        super.onCreate();
        RealmConfiguration configuration = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(configuration);
        BaseMvpComponent.context = this;
    }
}
