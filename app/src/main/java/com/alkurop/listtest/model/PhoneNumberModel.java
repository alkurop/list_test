package com.alkurop.listtest.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by alkurop on 9/5/16.
 */
public class PhoneNumberModel extends RealmObject {
    @PrimaryKey
    public String phoneNumber;
    public String phoneNumberPrice;
    public String phoneNumberOwner;

}
