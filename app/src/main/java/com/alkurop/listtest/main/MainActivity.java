package com.alkurop.listtest.main;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;

import com.alkurop.listtest.R;
import com.alkurop.listtest.model.PhoneNumberModel;
import com.github.alkurop.mvpbase.BaseActivity;
import com.github.alkurop.mvpbase.IBaseScreenPresenter;
import com.github.alkurop.updatinglist.UpdatingListView;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;

public class MainActivity extends BaseActivity implements IMainView {

    @BindView (R.id.mainContainer)
    CoordinatorLayout mainContainer;

    @BindView (R.id.listView)
    UpdatingListView listView;

    IMainPres presenter;
    MainAdapter adapter = new MainAdapter();

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPres(new MainInter(), this);
        initList();
    }

    private void initList () {
        listView.setLoadingViews(R.layout.list_native_loading_view, R.layout.list_empty_view);
        listView.setAdapter(adapter);
        listView.setSwipeRefreshListener(new Function0<Unit>() {
            @Override
            public Unit invoke () {
                presenter.refresh();
                return null;
            }
        });
    }

    @NotNull
    @Override
    public View getSnackbarAnchor () {
        return mainContainer;
    }

    @Nullable
    @Override
    public IBaseScreenPresenter getPresenter () {
        return presenter;
    }

    @Override
    public void addItems (List<? extends PhoneNumberModel> list, int i) {
        adapter.addItems(list);
    }

    @Override
    public void clearAdapter () {
        adapter.clear();
    }

    @Override
    public void onGetItemsError () {
        listView.onError();
    }

    @Override
    public void onRetry () {

    }

    @Override
    public void setLoading (boolean b) {
        listView.showLoading(b);
    }
}
