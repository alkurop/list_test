package com.alkurop.listtest.main;

import com.alkurop.listtest.MySubscriber;
import com.alkurop.listtest.model.PhoneNumberModel;
import com.github.alkurop.mvpbase.IBaseInteractor;

import java.util.List;

/**
 * Created by alkurop on 9/5/16.
 */
public interface IMainInter extends IBaseInteractor {
    void loadPhoneList (MySubscriber<Void> subscriber);
    void setPhoneNumberChangeListener(PhoneNumberChangeListener listener);
    void removePhoneNumberChangeListener();

    interface PhoneNumberChangeListener {
        void onChange (List<PhoneNumberModel> numbers);
    }
}
