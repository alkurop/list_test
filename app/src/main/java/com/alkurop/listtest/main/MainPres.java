package com.alkurop.listtest.main;

import com.alkurop.listtest.MySubscriber;
import com.alkurop.listtest.model.PhoneNumberModel;
import com.github.alkurop.mvpbase.BaseMvpComponent;
import com.github.alkurop.mvpbase.BaseScreenPresenter;
import com.github.alkurop.mvpbase.utils.errorhandling.DefaultErrorHandlerKt;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alkurop on 9/5/16.
 */
public class MainPres extends BaseScreenPresenter<IMainInter, IMainView> implements IMainPres {
    List<PhoneNumberModel> numbers = new ArrayList<>();

    public MainPres (IMainInter interactor, IMainView view) {
        super(interactor, view);
    }

    @Override
    public void onResume () {
        super.onResume();
        loadListFromDb();
        if (numbers.isEmpty()) {
            loadListFromInternet();
        }
    }

    private void loadListFromDb () {
        getInteractor().setPhoneNumberChangeListener(new IMainInter.PhoneNumberChangeListener() {
            @Override
            public void onChange (List<PhoneNumberModel> numbers) {
                addPhoneItems(numbers);
            }
        });
    }

    private void loadListFromInternet () {
        if (view.checkNetworkConnection()) {
            view.setLoading(true);
            getInteractor().loadPhoneList(new MySubscriber<Void>() {
                @Override
                public void onResult (Void result) {

                }

                @Override
                public void onError (Throwable t) {
                    DefaultErrorHandlerKt.handleSelf(t, view);
                    view.onGetItemsError();
                }

                @Override
                public void onTerminate () {
                    view.setLoading(false);
                }
            });

        } else view.showSnack(BaseMvpComponent.INSTANCE.getNoInternetErrorRes());
    }

    private void addPhoneItems (List<PhoneNumberModel> numbers) {
        this.numbers = numbers;
        view.clearAdapter();
        view.addItems(numbers, numbers.size());
        view.setLoading(false);
    }

    @Override
    public void refresh () {
        loadListFromInternet();
    }

    @Override
    public void onPause () {
        super.onPause();
        getInteractor().removePhoneNumberChangeListener();
    }
}
