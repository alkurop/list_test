package com.alkurop.listtest.main;

import com.alkurop.listtest.model.PhoneNumberModel;
import com.github.alkurop.mvpbase.IBaseScreenView;
import com.github.alkurop.updatinglist.AdapterListener;

/**
 * Created by alkurop on 9/5/16.
 */
public interface IMainView extends IBaseScreenView, AdapterListener<PhoneNumberModel> {

}
