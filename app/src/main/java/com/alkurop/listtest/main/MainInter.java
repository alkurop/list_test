package com.alkurop.listtest.main;

import com.alkurop.listtest.MySubscriber;
import com.alkurop.listtest.model.PhoneNumberModel;
import com.alkurop.listtest.network.NetworkModule;
import com.github.alkurop.mvpbase.BaseInteractor;
import com.github.alkurop.mvpbase.utils.NetworkUtilsKt;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.adapter.rxjava.Result;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by alkurop on 9/5/16.
 */
public class MainInter extends BaseInteractor implements IMainInter {
    Realm realm ;
    RealmResults<PhoneNumberModel> numberModels;
    private RealmChangeListener<RealmResults<PhoneNumberModel>> realmPhoneListChangeListener;


    @Override
    public void loadPhoneList (final MySubscriber<Void> subscriber) {
        Subscription sub =  NetworkUtilsKt.errorHandling( NetworkModule.api.getNumbers())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate(new Action0() {
                    @Override
                    public void call () {
                        subscriber.onTerminate();
                    }
                })
                .subscribe(new Action1<List<PhoneNumberModel>>() {
                    @Override
                    public void call (final List<PhoneNumberModel> phoneNumberModels) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute (Realm realm) {
                                realm.copyToRealmOrUpdate(phoneNumberModels);
                            }
                        });
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call (Throwable throwable) {
                        subscriber.onError(throwable);
                    }
                });

        subscribe(sub);
    }

    @Override
    public void setPhoneNumberChangeListener (final PhoneNumberChangeListener listener) {
        realm = Realm.getDefaultInstance();
        realmPhoneListChangeListener = new RealmChangeListener<RealmResults<PhoneNumberModel>>() {
            @Override
            public void onChange (RealmResults<PhoneNumberModel> element) {
                listener.onChange(realm.copyFromRealm(element));
            }
        };
        numberModels = realm.where(PhoneNumberModel.class).findAll();
        numberModels.addChangeListener(realmPhoneListChangeListener);
        listener.onChange(realm.copyFromRealm(numberModels));
    }

    @Override
    public void removePhoneNumberChangeListener () {
        numberModels.removeChangeListener(realmPhoneListChangeListener);
    }


    @Override
    public void unsubscribe () {
        super.unsubscribe();
        if (numberModels != null && realmPhoneListChangeListener != null) {
            numberModels.removeChangeListener(realmPhoneListChangeListener);
        }
        if (realm != null) {
            realm.close();
        }
    }
}
