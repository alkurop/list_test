package com.alkurop.listtest.main;

import com.github.alkurop.mvpbase.IBaseScreenPresenter;

/**
 * Created by alkurop on 9/5/16.
 */
public interface IMainPres extends IBaseScreenPresenter {
    void refresh ();
}
