package com.alkurop.listtest.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alkurop.listtest.R;
import com.alkurop.listtest.model.PhoneNumberModel;
import com.github.alkurop.updatinglist.BaseLoadMoreAdapter;
import com.github.alkurop.updatinglist.BaseViewHolder;

import org.jetbrains.annotations.NotNull;

import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by alkurop on 9/5/16.
 */
public class MainAdapter extends BaseLoadMoreAdapter<PhoneNumberModel>{
    @NotNull
    @Override
    public BaseViewHolder<PhoneNumberModel> onCreateProgressVH (ViewGroup viewGroup) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_empty_view, viewGroup, false);
        return new ProgressVH(v);
    }

    @NotNull
    @Override
    public BaseViewHolder<PhoneNumberModel> onCreateVH (ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_view, viewGroup, false);
        return new ListItemVh(v);
    }
    class ListItemVh extends BaseViewHolder<PhoneNumberModel> {
        @BindViews ({R.id.number, R.id.owner, R.id.price})
        TextView[] textViews;
        public ListItemVh (View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bind (PhoneNumberModel data) {
            super.bind(data);
            textViews[0].setText(data.phoneNumber);
            textViews[1].setText(data.phoneNumberOwner);
            textViews[2].setText(data.phoneNumberPrice);
        }
    }
}
